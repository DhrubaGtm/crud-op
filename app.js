var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan')
const cors = require('cors')
const authenticate = require('./middleware/authenticate')
const bodyparse = require('body-parser');

var app = express();
const db = require('./db');
var model = require('./models/user.models');
varformmodel = require('./models/newform.model');


var authRouter = require('./routes/auth');
var usersRouter = require('./routes/users');
var fillformRouter = require('./routes/fillForm');

app.use(bodyparse.urlencoded({ extended: true }));
app.use(bodyparse.json());
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');


app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(cors());

app.use('/auth', authRouter);
app.use('/users', authenticate, usersRouter);
app.use('/fill', fillformRouter);

// catch 404 and forward to error handler
app.use(function (err, req, res, next) {
  console.log('i am error handling middleware', err);
  res.status(err.status || 400);
  res.json({
    msg: 'from error handling middleware',
    err: err
  })
});

// error handler
app.use(function (err, req, res, next) {
  console.log('i am error handling middleware', err);
  res.json({
    msg: 'from error handling middleware',
    err: err
  })
});

module.exports = app;

