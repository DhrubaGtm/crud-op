const mongoose = require('mongoose');

const schema = mongoose.Schema;

const formSchema = new schema({
    firstname: String,
    middlename: String,
    lastname: String,
    dob: Date,
    bloodgroup: String,
    citizenship:{
        type:String,
        default: 'nepali'
    },
    citizenshipno: String,
    citizenshipIssueDistrict:{
        type: String
    },
    witnessFN: String,
    witnessMN: String,
    witnessLN: String,
    witnessRelationship: String,

    Pzone: String,
    Pdistrict: String,
    Pvillage: String,
    PwardNo: Number,
    Ptole: String,
    PmobileNo: Number,

    pzone: String,
    pdistrict: String,
    pvillage: String,
    pwardNo: Number,
    ptole: String,
    pmobileNo:{
        type: Number,
        
    },

    selectCategory: String,
    selectZone: String,
    licenceIssueOffice: String


})

const formModel = mongoose.model('newforms', formSchema);
module.exports = formModel;