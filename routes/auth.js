var express = require('express');
var router = express.Router();
const jwt = require ('jsonwebtoken')
const passwordHash = require('password-hash');
var userModel = require('./../models/user.models');
var config = require('../config/index');


router.post('/register', function (req, res, next) {
  var newUser = new userModel({})
  if (req.body.username)
    newUser.username = req.body.username;
  if (req.body.password)
    newUser.password = passwordHash.generate(req.body.password)

  console.log('new user>>', newUser)
  newUser.save(function (err, done) {
    if (err) {
      return next(err);
    }
    res.json(done).status(200);
  });
});

router.post('/login', function (req, res, next) {
  var newUser = new userModel({})
  userModel.findOne({
    username: req.body.username
  }, function (err, user) {
    if (err) {
      return next(err);
    } if (user) {
      var isMatch = passwordHash.verify(req.body.password, user.password);
      if (isMatch) {
        var token = jwt.sign({ id: user._id, name: user.username }, config.jwtSecret);
        console.log('token>>', token);
        res.status(200).json(
          {
            user: user,
            token: token
          }
        );
      } else {
        next({
          msg: "invalid username and password"
        })
      }
    }
  })
});

module.exports = router;
