var express = require('express');
var router = express.Router();
var userModel = require('./../models/user.models');

router.get('/', function (req, res, next) {
  userModel
    .find({


    })
    .sort({
      _id: -1
    })
    .exec(function (err, done) {
      if (err) {
        return next(Err)
      }
      res.json(done).status(200);
    })
});
router.get('/:id', function (req, res, next) {
  userModel.findById(req.params.id, function (err, done) {
    if (err) {
      return next(err);
    }
    res.status(200).json(done);
  })
})
router.delete('/:id', function (req, res, next) {
  userModel.findById(req.params.id, function (err, user) {
    if (err) {
      return next(err);
    }
    if (user) {
      user.remove(function (err, user) {
        if (err) {
          return next(err);
        }
        res.status(200).json(user);
      })
    }
    else {
      next({
        msg: 'not found'
      })

    }
  })
})
router.put('/:id', function (req, res, next) {
  userModel.findById(req.params.id, function (err, user) {
    if (err) {
      return next(err);
    } if (user) {
      res.status(200).json(user)
    } else {
      next({
        msg: 'can not find user'
      })
    }
    if (req.body.username)
      user.username = req.body.username;
    if (req.body.password)
      user.password = req.body.password;

    user.save(function (err, user) {
      if (err) {
        return next(err);
      }
      res.json(user).status(200);
    })
  })
})

module.exports = router;
