var express = require('express');
var router = express.Router();
var formModel = require('./../models/newform.model');


router.post('/fillform', function (req, res, next) {
    var newform = new formModel({})
    if (req.body.firstname)
        newform.firstname = req.body.firstname;
    if (req.body.middlename)
        newform.middlename = req.body.middlename;
    if (req.body.lastname)
        newform.lastname = req.body.lastname;

    if (req.body.dob)
        newform.dob = req.body.dob;

    if (req.body.bloodgroup)
        newform.bloodgroup = req.body.bloodgroup;

    if (req.body.citizenship)
        newform.citizenship = req.body.citizenship;

    if (req.body.citizenshipno)
        newform.citizenshipno = req.body.citizenshipno;

    if (req.body.citizenshipIssueDistrict)
        newform.citizenshipIssueDistrict = req.body.citizenshipIssueDistrict;

    if (req.body.witnessFN)
        newform.witnessFN = req.body.witnessFN;
    if (req.body.witnessMN)
        newform.witnessMN = req.body.witnessMN;
    if (req.body.witnessLN)
        newform.witnessLN = req.body.witnessLN;
    if (req.body.witnessRelationship)
        newform.witnessRelationship = req.body.witnessRelationship;


    if (req.body.Pzone)
        newform.Pzone = req.body.Pzone;
    if (req.body.Pdistrict)
        newform.Pdistrict = req.body.Pdistrict;
    if (req.body.Pvillage)
        newform.Pvillage = req.body.Pvillage;
    if (req.body.PwardNo)
        newform.PwardNo = req.body.PwardNo;
    if (req.body.Ptole)
        newform.Ptole = req.body.Ptole;
    if (req.body.PmobileNo)
        newform.PmobileNo = req.body.PmobileNo;


    if (req.body.pzone)
        newform.pzone = req.body.pzone;
    if (req.body.pdistrict)
        newform.pdistrict = req.body.pdistrict;
    if (req.body.pvillage)
        newform.pvillage = req.body.pvillage;
    if (req.body.pwardNo)
        newform.pwardNo = req.body.pwardNo;
    if (req.body.ptole)
        newform.ptole = req.body.ptole;
    if (req.body.pmobileNo)
        newform.pmobileNo = req.body.pmobileNo;


    if (req.body.selectCategory)
        newform.selectCategory = req.body.selectCategory;
    if (req.body.selectZone)
        newform.selectZone = req.body.selectZone;
    if (req.body.licenceIssueOffice)
        newform.licenceIssueOffice = req.body.licenceIssueOffice;


    console.log('new Form>>', newform)
    newform.save(function (err, done) {
        if (err) {
            return next(err);
        }
        res.json(done).status(200);
    });
});
router.get('/', function (req, res, next) {
    formModel
        .find({


        })
        .sort({
            _id: -1
        })
        .exec(function (err, done) {
            if (err) {
                return next(err)
            }
            res.json(done).status(200);
        })
});

router.get('/:id', function (req, res, next) {
    formModel.findById(req.params.id, function (err, done) {
        if (err) {
            return next(err);
        }
        res.status(200).json(done);
    })
})
router.delete('/:id', function (req, res, next) {
    formModel.findById(req.params.id, function (err, form) {
        if (err) {
            return next(err);
        }
        if (form) {
            form.remove(function (err, form) {
                if (err) {
                    return next(err);
                }
                res.status(200).json(form);
            })
        }
        else {
            next({
                msg: 'not found'
            })

        }
    })
})
router.put('/:id', function (req, res, next) {
    formModel.findById(req.params.id, function (err, form) {
        if (err) {
            return next(err);
        } if (form) {
            res.status(200).json(form)
        } else {
            next({
                msg: 'can not find form'
            })
        }
        if (req.body.firstname)
            form.firstname = req.body.firstname;
        if (req.body.middlename)
            form.middlename = req.body.middlename;
        if (req.body.lastname)
            form.lastname = req.body.lastname;

        if (req.body.dob)
            form.dob = req.body.dob;

        if (req.body.bloodgroup)
            form.bloodgroup = req.body.bloodgroup;

        if (req.body.citizenship)
            form.citizenship = req.body.citizenship;

        if (req.body.citizenshipno)
            form.citizenshipno = req.body.citizenshipno;

        if (req.body.citizenshipIssueDistrict)
            form.citizenshipIssueDistrict = req.body.citizenshipIssueDistrict;

        if (req.body.witnessFN)
            form.witnessFN = req.body.witnessFN;
        if (req.body.witnessMN)
            form.witnessMN = req.body.witnessMN;
        if (req.body.witnessLN)
            form.witnessLN = req.body.witnessLN;
        if (req.body.witnessRelationship)
            form.witnessRelationship = req.body.witnessRelationship;


        if (req.body.Pzone)
            form.Pzone = req.body.Pzone;
        if (req.body.Pdistrict)
            form.Pdistrict = req.body.Pdistrict;
        if (req.body.Pvillage)
            form.Pvillage = req.body.Pvillage;
        if (req.body.PwardNo)
            form.PwardNo = req.body.PwardNo;
        if (req.body.Ptole)
            form.Ptole = req.body.Ptole;
        if (req.body.PmobileNo)
            form.PmobileNo = req.body.PmobileNo;


        if (req.body.pzone)
            form.pzone = req.body.pzone;
        if (req.body.pdistrict)
            form.pdistrict = req.body.pdistrict;
        if (req.body.pvillage)
            form.pvillage = req.body.pvillage;
        if (req.body.pwardNo)
            form.pwardNo = req.body.pwardNo;
        if (req.body.ptole)
            form.ptole = req.body.ptole;
        if (req.body.pmobileNo)
            form.pmobileNo = req.body.pmobileNo;


        if (req.body.selectCategory)
            form.selectCategory = req.body.selectCategory;
        if (req.body.selectZone)
            form.selectZone = req.body.selectZone;
        if (req.body.licenceIssueOffice)
            form.licenceIssueOffice = req.body.licenceIssueOffice;

        newform.save(function (err, form) {
            if (err) {
                return next(err);
            }
            res.json(form).status(200);
        })
    })
})

module.exports = router;

